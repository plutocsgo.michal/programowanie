import tkinter as tk #jest używany do tworzenia graficznego interfejsu użytkownika (GUI)
from tkinter import ttk
import requests
import pickle
import os


class CurrencyConverter:
    def __init__(self, master):
        self.master = master
        master.title("Konwerter Walut") #tytuł główny aplikacji
        self.currency_data = self.load_currency_data() #wczytuje aktualne kursy

        #elementy interfejsu użytkownika
        self.source_currency_var = tk.StringVar() #przechowuje źródłową walutę
        self.target_currency_var = tk.StringVar() #przechowuje docelową walutę
        self.amount_entry = tk.Entry(master, width=10) #kwota do przeliczenia
        self.converted_amount_label = ttk.Label(master, text="Wynik: ")
        self.convert_button = ttk.Button(master, text="Przelicz", command=self.convert)
        self.quit_button = ttk.Button(master, text="Zakończ", command=master.quit)

        #rozmieszczenie elementów
        ttk.Label(master, text="Wybierz walutę źródłową:").grid(column=0, row=0)
        ttk.Label(master, text="Wybierz walutę docelową:").grid(column=0, row=1)
        ttk.Label(master, text="Kwota:").grid(column=0, row=2)
        ttk.Combobox(master, textvariable=self.source_currency_var, values=list(self.currency_data.keys())).grid(column=1, row=0)
        ttk.Combobox(master, textvariable=self.target_currency_var, values=list(self.currency_data.keys())).grid(column=1, row=1)
        self.amount_entry.grid(column=1, row=2)
        self.converted_amount_label.grid(column=1, row=3)
        self.convert_button.grid(column=1, row=4)
        self.quit_button.grid(column=1, row=5)

    def load_currency_data(self): #próbujemy wczytać nasz juz zapisany plik z kursami, jesli go nie ma pobieramy dane z internetu
        if os.path.exists('currency_data.pkl'):
            try:
                with open('currency_data.pkl', 'rb') as f:
                    return pickle.load(f)
            except Exception as e:
                print(f"Błąd podczas wczytywania danych z pliku: {e}")
        return self.update_currency_data()

    def update_currency_data(self): #pobiera dane z internetu o kursach walut i przechowuje je w pliku
        try:
            response = requests.get('http://api.nbp.pl/api/exchangerates/tables/A/', timeout=5)
            response.raise_for_status()
            data = response.json()[0]['rates']
            currency_data = {item['code']: item['mid'] for item in data}
            with open('currency_data.pkl', 'wb') as f:
                pickle.dump(currency_data, f)
            return currency_data
        except (requests.RequestException, ValueError) as e:
            print("Błąd podczas pobierania danych. Używam lokalnych danych z ostatniego pobrania.")
            return {}

    def convert(self): #pobiera wartości z interfejsu, wykonuje konwersję walut na podstawie wczytanych kursów i zwraca wynik
        amount = float(self.amount_entry.get())
        source_rate = self.currency_data[self.source_currency_var.get()]
        target_rate = self.currency_data[self.target_currency_var.get()]
        result = amount * (source_rate / target_rate)
        self.converted_amount_label.config(text=f"Wynik: {result:.2f}") #fstring do 2 miejsc po przecinku


root = tk.Tk() #tworzy główne okno aplikacji
app = CurrencyConverter(root) #instancja naszej klasy i interfejs
root.mainloop() #interaktywne okno aplikacji

