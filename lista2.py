import random
import string
#zadanie 1
def losowe_haslo(dlugosc=8):
    znaki = string.ascii_letters + string.digits + string.punctuation
    haslo = ''.join(random.choice(znaki) for _ in range(dlugosc)) #łączy wszystkie wybrane znaki w ciąg znaków
    return haslo
haslo=losowe_haslo()
print("zadanie 1")
print(haslo)

#zadanie 2
from PIL import Image
im = Image.open("sokol_millenium.jpg") #wczytujemy grafikę
im.thumbnail((128, 128)) #tworzymy miniature o określonych rozmiarach
im.save('maly.png', "PNG")

#zadanie 3
import os
import zipfile
from datetime import datetime

def backup_katalogu(sciezka_katalogu):
    nazwa_archiwum = datetime.now().strftime('%Y%m%d') + '_backup.zip' #nadajemy nazwę katalogowi
    with zipfile.ZipFile(nazwa_archiwum, 'w') as zipf: # 'w' nadpisanie katalogu, 'with' zapewnia automatyczne zamkniecie po skonczeniu operacji
        for root, dirs, files in os.walk(sciezka_katalogu): #zwraca krotki katalog_aktualny, lista_podkatalogów, lista_plików itreujac przez sciezke
            for file in files:
                zipf.write(os.path.join(root, file)) #dodaje plik do zipa
    return nazwa_archiwum

print("zadanie 3")
print(backup_katalogu('C:/Users/micha/OneDrive/Desktop/Analiza'))

#zadanie 4
from PyPDF2 import PdfReader, PdfWriter

def podziel_pdf(sciezka_pdf, liczby_stron):
    reader = PdfReader(sciezka_pdf) #tworzymy czytnik PDF
    startowa_strona = 0 #licznik stron

    for i, liczba_stron in enumerate(liczby_stron):
        writer = PdfWriter() #zapisuje strony do nowego pliku
        if startowa_strona >= len(reader.pages):
            print(f"Nie ma więcej stron do zapisania. Zapisano {i} plików.")
            break

        for j in range(liczba_stron):
            if startowa_strona < len(reader.pages):
                writer.add_page(reader.pages[startowa_strona]) #dodaje strone do obiektu 'writer'
                startowa_strona += 1
            else:
                break

        with open(f'czesc_{i + 1}.pdf', 'wb') as f: #otwiera nowy plik PDF w trybie zapisu binarnego('wb')
            writer.write(f)
        print("zadanie 4")
        print(f'Zapisano czesc_{i + 1}.pdf')

podziel_pdf('tekst.pdf', [3, 3, 1])

#zadanie 5
from PIL import Image

image = Image.open('ptaki1.jpg') #główna grafika
logo = Image.open('python_logo.png') #znak wodny
image_copy = image.copy() #kopia, abyśmy nie modyfikowali oryginalnego obrazu
position = ((image_copy.width - logo.width), (image_copy.height - logo.height))
image_copy.paste(logo, position, logo) #trzecia zmienna pozwala nam na przezroczytosc dzieki masce
image_copy.save('znak_wodny.jpg')


#zadanie 6
def generuj_slupek(dzialanie):

    liczby = dzialanie.split('+') #dzieli łańcuch dzialanie na poszczególne liczby według znaku '+' i przechowuje je jako listę w zmiennej 'liczby'
    szerokosc = max(len(liczby[0]), len(liczby[1]) +2)  #obliaczmy szerokość słupka na podstawie najdłuższego elementu
    wynik = str(eval(dzialanie)) #oblicza wynik działania matematycznego, które jest przekazane jako łańcuch znaków 'dzialanie', za pomocą funkcji eval(). Wynik jest następnie konwertowany na łańcuch znaków i przechowywany w zmiennej 'wynik'
    linia = '-' * (szerokosc)
    pierwsza_liczba = liczby[0].rjust(szerokosc) #wyrównujemy do prawej strony
    druga_liczba = ('+ ' + liczby[1]).rjust(szerokosc) #-||-
    slupek = f"{pierwsza_liczba}\n{druga_liczba}\n{linia}\n{wynik.rjust(szerokosc)}" #zwraca łańcuch znaków reprezentujących słupek
    return slupek

dzialanie = "235+1"
slupek = generuj_slupek(dzialanie)
print("zadanie 6")
print(slupek)