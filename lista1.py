#zadanie 1
def rgb_html(rgb):
    for wartosc in rgb:
        if wartosc > 255 or wartosc < 0: # sprawdzamy czy nasza liczba znajduje się w dobrym zakresie
            return ("Wartości kolorów RGB muszą być w zakresie od 0 do 255.")

    html_rgb = '#{:02x}{:02x}{:02x}'.format(rgb[0], rgb[1], rgb[2])
    return html_rgb # zamieniamy kolor z formatu RGB na HTML, za pomoca zamiany liczb z systemu dziesietnego na szesznastkowy

print(rgb_html((255, 0, 255)))
print(rgb_html((200, 128, 255)))

#zadanie 2
def html_rgb(html):
    if not (html.startswith("#") and len(html) == 7):
        return ("Wartości kolorów HTML muszą zaczynać się od znaku # i muszą muszą się składać z 7 znaków.")
    a = int(html[1:3], 16)
    b = int(html[3:5], 16)
    c = int(html[5:7], 16)

    return a,b,c

print(html_rgb("#ff00ff"))
print(html_rgb("#000000"))

#zadanie 3
def Fibo(n):
    liczby = []
    if n <= 0:
        return ("niemożliwe")
    if n == 1:
        liczby = [1]
    else:
        liczby = [1,1]
        for i in range(2,n):
            liczby.append(liczby[i-2]+liczby[i-1])

    return liczby
print(Fibo(20))

#zadanie 4
def Fibo(n):
    liczby=[]
    if n<=0:
        return("Niemożliwe dla takich wartości")
    if n == 1:
        liczby = [1]
    else:
        liczby=[1]
        fibo_1=0
        fibo_2=1

        for i in range(2,n+1):
            fibo_3=fibo_1 + fibo_2
            fibo_1=fibo_2
            fibo_2=fibo_3
            liczby.append(fibo_2)
        return liczby
print(Fibo(10))

#funkcja iteracyjna jest o wiele szybsza niż funkcja rekurencyjna.

#zadanie 5
def palindrom(slowo):
    if slowo == slowo[::-1]: #idziemy od ostatniej do pierwszej litery
        return True
    else:
        return False
print(palindrom("kajak"))

#zadanie 6
def sprawdz(lista):
    return lista == sorted(lista)
print(sprawdz(lista=[1,5,20,193]))

#zadanie 7
def angaram(slowo1,slowo2):
    if not len(slowo1) == len(slowo2):
        return("Podane słowa nie są anagramami")
    if len(slowo1) == len(slowo2):
        licznik_slowo1={}
        licznik_slowo2={}
        for litera in slowo1:
            licznik_slowo1[litera]=licznik_slowo1.get(litera, 0) + 1 #funkja .get sprawdza czy podana litera wystepuje juz w slowniku, jesli nie to przypisujemy jej wartosc rowna 1 a jesli istnieje to dodajemy do wartosci 1

        for litera in slowo2:
            licznik_slowo2[litera]= licznik_slowo2.get(litera, 0) + 1
#litery są kluczami a wartosciami jest liczba wystapien
        if licznik_slowo1 == licznik_slowo2:
            return ("podane słowa są angramami")
        else:
            return("podane słowa nie są anagramami")

print(angaram("jelen","lejen"))








