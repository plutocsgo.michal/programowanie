#zadanie 1
import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import sys

def seir_model(t, y, N, beta, sigma, gamma):#czas nie jest uzywany w rownaniach ale jest potrzebny do funkcji solve_ivp
    S, E, I, R = y
    dSdt = -beta * S * I / N
    dEdt = beta * S * I / N - sigma * E
    dIdt = sigma * E - gamma * I
    dRdt = gamma * I
    return [dSdt, dEdt, dIdt, dRdt]

def run_seir_simulation(N, S0, E0, I0, R0, beta, sigma, gamma, days):
    initial_conditions = [S0, E0, I0, R0]#poczatkowa liczba ludzi
    t = np.linspace(0, days, days + 1)
    result = solve_ivp(seir_model, [0, days], initial_conditions, args=(N, beta, sigma, gamma), t_eval=t)
    return t, result.y

def plot_seir(t, data):
    plt.figure(figsize=(10, 6))
    plt.plot(t, data[0], label='Podatni (S)')
    plt.plot(t, data[1], label='Narażeni (E)')
    plt.plot(t, data[2], label='Zakażeni (I)')
    plt.plot(t, data[3], label='Ozdrowieńcy (R)')
    plt.title('Ewolucja epidemii w modelu SEIR')
    plt.xlabel('Dni')
    plt.ylabel('Liczba osób')
    plt.legend()
    plt.grid(True)
    plt.show()

if __name__ == '__main__':
    if len(sys.argv) < 9: #sprawdza czy podano dobra liczbe argumentow
        print("Użycie: python seir.py N S0 E0 I0 R0 beta sigma gamma")
        #python seir.py 1000 999 1 0 0 1.34 0.19 0.34
    else:
        N = int(sys.argv[1])
        S0 = int(sys.argv[2])
        E0 = int(sys.argv[3])
        I0 = int(sys.argv[4])
        R0 = int(sys.argv[5])
        beta = float(sys.argv[6])
        sigma = float(sys.argv[7])
        gamma = float(sys.argv[8])
        days = 160  #domyślna liczba dni symulacji

        t, data = run_seir_simulation(N, S0, E0, I0, R0, beta, sigma, gamma, days)
        plot_seir(t, data)