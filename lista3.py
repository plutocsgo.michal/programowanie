#zadanie 1
print("zadanie 1")
import os
from datetime import datetime, timedelta
import shutil

def tworz_backup_plikow(katalog_wyszukiwania, rozszerzenia, dni=3):
    aktualna_data = datetime.now().strftime('%Y-%m-%d')
    nazwa_folderu_backup = os.path.join('Backup', f'copy-{aktualna_data}') #stworzenie folderu z datą
    dom = 'C:/users/micha' #ścieżka, na którą ma trafić nasz backup
    sciezka_do_folderu_backup = os.path.join(dom, nazwa_folderu_backup)

    if os.path.exists(sciezka_do_folderu_backup): #sprawdzamy czy folder już istnieje
        raise FileExistsError(f"Folder backup '{sciezka_do_folderu_backup}' już istnieje.")

    os.makedirs(sciezka_do_folderu_backup) #tworzenie folderu jeśli taki nie istnieje
    granica_czasowa = datetime.now() - timedelta(days=dni) #data graniczna do 3 dni przed

    for foldername, subfolders, filenames in os.walk(katalog_wyszukiwania): #przechodzi przez cały katalog, foldery, podfoldery i pliki
        for filename in filenames:
            if any(filename.endswith(rozszerzenie) for rozszerzenie in rozszerzenia): #sprawdza czy jakiś plik pasuje do naszego rozszerzenia
                sciezka_pliku = os.path.join(foldername, filename) #tworzy pełną ścieżkę do pliku łącząc nazwę katalogu z nazwą pliku
                czas_modyfikacji = datetime.fromtimestamp(os.path.getmtime(sciezka_pliku)) #wczytuje czas modyfikacji
                if czas_modyfikacji > granica_czasowa: #czas modyfikacji mniejszy niż 3 dni
                    sciezka_docelowa = os.path.join(sciezka_do_folderu_backup, filename) #dodaje pliki do folderu backup
                    shutil.copy2(sciezka_pliku, sciezka_docelowa) #kopiowanie pliku
                    print(f'Kopia zapasowa {filename} została utworzona w {sciezka_docelowa}')

katalog_wyszukiwania = 'C:/users/micha/hej'
rozszerzenia = ['.txt']
try:
    tworz_backup_plikow(katalog_wyszukiwania, rozszerzenia)
except FileExistsError as e:
    print(e)


#zadanie 2
print('zadanie 2')
def convert_line_endings(filename):
    with open(filename, 'rb') as file: #otwieramy plik do czytania go w trybie binarnym
        content = file.read()

    print("Oryginalna treść binarna:", content)

    if b'\r\n' in content: #sprawdzamy typ końca linii
        new_content = content.replace(b'\r\n', b'\n') #znalezione końce linii Windows zamień na Unix
        print("Zamieniono końce linii Windows na Unix.")
    else:
        new_content = content.replace(b'\n', b'\r\n') #znalezione końce linii Unix zamień na Windows
        print("Zamieniono końce linii Unix na Windows.")
    print("Zmodyfikowana treść binarna:", new_content)

    with open(filename, 'wb') as file: #zapisz zmodyfikowaną zawartość z powrotem do pliku
        file.write(new_content)
    print(f"Zaktualizowano plik: {filename}")

convert_line_endings('C:/users/micha/hej/ez.txt')

#zadanie 3
print('zadanie 3')
from PyPDF2 import PdfReader, PdfWriter
def polacz_pdf(paths, output_path):
    pdf_writer = PdfWriter()

    for path in paths:
        pdf_reader = PdfReader(path)
        for page in range(len(pdf_reader.pages)):
            pdf_writer.add_page(pdf_reader.pages[page])

    with open(output_path, 'wb') as out: #otwiera plik do zapisu w trybie binarnym
        pdf_writer.write(out)

    print(f"Połączone dokumenty PDF zapisane jako: {output_path}")
pdf_paths = ['czesc_1.pdf', 'czesc_2.pdf', 'czesc_3.pdf']
output_pdf_path = 'polaczony_dokument.pdf'
polacz_pdf(pdf_paths, output_pdf_path)

#zadanie 4
print("zadanie 4")
import qrcode
def generate_qr(data, filename='myqr.png'):
    qr = qrcode.QRCode(
        version=1, #najmniejszy rodzaj siatki
        error_correction=qrcode.constants.ERROR_CORRECT_H, #ustawia wysoki poziom korekcji błędów
        box_size=10, #określa, że kazdy box bedzie mial wymairy 10x10 pikseli
        border=4, #ustawia szerokość marginesu wokół kodu na 4 moduły
    )
    qr.add_data(data) #dodaje przekazane dane do obiektu
    qr.make(fit=True) #genreuje finalny kod qr tak, aby pasował do danych
    img = qr.make_image(fill_color="black", back_color="white") #tworzy obraz kodu QR z czarnymi pikselami na białym tle
    img.save(filename)
    print(f"Kod QR został zapisany jako {filename}")

import cv2
def read_qr(filename):

    img = cv2.imread(filename) #wczytuje obraz z pliku
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) #konwertuje obraz do odcieni szarości. Konwersja ta jest potrzebna, ponieważ detekcja kodu QR często wymaga obrazu w skali szarości
    detector = cv2.QRCodeDetector() #obiekt, który bedzie dekodował nasz kod QR
    data, points, _ = detector.detectAndDecode(gray_img) #data- zawiera zdekodowane dane kodu QR, points- współrzędne punktów kodu QR,  _- zawiera surowe dane z kodu
    if points is not None:
        print(f"Dane z kodu QR: {data}")
    else:
        print("Nie znaleziono kodu QR w pliku.")

generate_qr("Hello, World!")
read_qr('cwiczenie.jpg')

#zadanie 5
print('zadanie 5')
def check_brackets(expression):
    stack = [] #stos do śledzenia otwartych nawiasów
    bracket_map = {')': '(', ']': '[', '}': '{'} #mapowanie nawiasów zamykających do otwierających
    for char in expression: #przechodzenie przez każdy znak w wyrażeniu
        if char in bracket_map.values(): #jeśli znak to nawias otwierający, dodaj go do stosu
            stack.append(char)
        elif char in bracket_map: #jeśli znak to nawias zamykający
            if not stack or bracket_map[char] != stack.pop(): #sprawdzamy, czy stos nie jest pusty i czy górny element stosu pasuje
                return False
    return not stack
expressions = ["(a + b) * [c - d]", "(a + b)) * [c - d]", "({a + b} * [c - d)]"]
for exp in expressions:
    print(f"Expression: {exp}\t - Valid: {check_brackets(exp)}")

#zadanie 6
print("zadanie 6")
import requests
from bs4 import BeautifulSoup
import webbrowser
def get_random_wikipedia_article():
    url = "https://en.wikipedia.org/wiki/Special:Random"
    response = requests.get(url) #przechowuje nasz losowy artykuł
    soup = BeautifulSoup(response.text, 'html.parser') #parsuje kod artykułu(przetwarza na surowy)
    title = soup.find('h1', id='firstHeading').text #wyszukuje w przetworzonym dokumencie HTML element h1 z identyfikatorem firstHeading, który zazwyczaj zawiera tytuł artykułu na Wikipedii
    return title, response.url #zwraca dwa elementy: tytuł artykułu oraz oryginalny URL, pod którym znajduje się ten artykuł
def main():
    title, url = get_random_wikipedia_article()
    print("Czy podoba Ci się tytuł tego artykułu? ", title)
    answer = input("Wpisz 'tak', jeśli chcesz otworzyć artykuł w przeglądarce: ").lower() #pobiera odpowiedź i konwertuje ją na małe litery
    if answer == 'tak':
        webbrowser.open(url)

if __name__ == "__main__":
    main()
