import networkx as nx
import matplotlib.pyplot as plt
import imageio.v2 as imageio
import cv2
import numpy as np
import os

def create_graph():
    #tworzymy losowy graf przy użyciu funkcji gnp_random_graph
    G = nx.gnp_random_graph(10, 0.5)  #graf z 10 węzłami i prawdopodobieństwem połączenia 0.5
    return G

def random_walk(G, steps=50, interval=1): #przeprowadza spacer losowy po grafie przez okreslona liczbe krokow
    node = np.random.choice(list(G.nodes())) #losuje wezel startowy
    positions = nx.spring_layout(G) #generuje uklad wezow
    for i in range(steps):
        if i % interval == 0:
            yield i, node, positions #generuje krotke zawierajaca numer kroku, aktualny wezel i pozycje wezlow
        neighbors = list(G.neighbors(node))
        node = np.random.choice(neighbors) if neighbors else node

def save_graph(G, node, positions, filename):#rysuje graf z zaznaczonym aktualnym wezlem 'node' na czerwono, reszta na szaro
    plt.figure(figsize=(8, 6))
    nx.draw(G, pos=positions, node_color='grey', with_labels=True, node_size=700)
    nx.draw_networkx_nodes(G, pos=positions, nodelist=[node], node_color='red')
    plt.savefig(filename)
    plt.close()

def generate_images(G, total_steps=50, interval=1):#generuje serie obrazow, przedstawiajacych kolejne etapy spaceru
    images = []
    for i, node, positions in random_walk(G, total_steps, interval):
        filename = f'frame_{i}.png'
        save_graph(G, node, positions, filename)
        images.append(filename)
    return images

def create_movie(images, output='movie.avi', fps=2): #tworzy plik video, korzystajac z opencv
    frame = cv2.imread(images[0])
    height, width, layers = frame.shape
    video = cv2.VideoWriter(output, cv2.VideoWriter_fourcc(*'DIVX'), fps, (width, height))
    for image in images:
        video.write(cv2.imread(image))
    cv2.destroyAllWindows()
    video.release()

def create_gif(images, output='movie.gif', fps=2): #tworzy gif
    with imageio.get_writer(output, mode='I', fps=fps) as writer:
        for image in images:
            writer.append_data(imageio.imread(image))

if __name__ == '__main__':
    G = create_graph()
    images = generate_images(G, 50, 5)
    create_movie(images)
    create_gif(images)
