#zadanie 2
import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import argparse

def seir_model(t, y, N, beta, sigma, gamma):
    S, E, I, R = y
    dSdt = -beta * S * I / N
    dEdt = beta * S * I / N - sigma * E
    dIdt = sigma * E - gamma * I
    dRdt = gamma * I
    return [dSdt, dEdt, dIdt, dRdt]

def run_seir_simulation(N, S0, E0, I0, R0, beta, sigma, gamma, days):
    initial_conditions = [S0, E0, I0, R0]
    t = np.linspace(0, days, days + 1)
    result = solve_ivp(seir_model, [0, days], initial_conditions, args=(N, beta, sigma, gamma), t_eval=t)
    return t, result.y

def plot_seir(t, data):
    plt.figure(figsize=(10, 6))
    plt.plot(t, data[0], label='Podatni (S)')
    plt.plot(t, data[1], label='Narażeni (E)')
    plt.plot(t, data[2], label='Zakażeni (I)')
    plt.plot(t, data[3], label='Ozdrowieńcy (R)')
    plt.title('Ewolucja epidemii w modelu SEIR')
    plt.xlabel('Dni')
    plt.ylabel('Liczba osób')
    plt.legend()
    plt.grid(True)
    plt.show()

def main(): #ustawiamy wartości domyślne
    parser = argparse.ArgumentParser(description="SEIR model epidemic simulation.")
    parser.add_argument('-N', type=int, default=1000, help='Total population')
    parser.add_argument('-S0', type=int, default=999, help='Initial number of susceptible individuals')#podatni
    parser.add_argument('-E0', type=int, default=0, help='Initial number of exposed individuals')#narazeni
    parser.add_argument('-I0', type=int, default=1, help='Initial number of infected individuals')#zakazeni
    parser.add_argument('-R0', type=int, default=0, help='Initial number of recovered individuals')#ozdrowieni
    parser.add_argument('-beta', type=float, default=1.0, help='Rate of infection')
    parser.add_argument('-sigma', type=float, default=0.2, help='Rate of progression from exposed to infectious')
    parser.add_argument('-gamma', type=float, default=0.1, help='Recovery rate')
    parser.add_argument('-days', type=int, default=160, help='Number of days to simulate')

    args = parser.parse_args()

    N = args.N
    S0 = args.S0
    E0 = args.E0
    I0 = args.I0
    R0 = args.R0
    beta = args.beta
    sigma = args.sigma
    gamma = args.gamma
    days = args.days

    t, data = run_seir_simulation(N, S0, E0, I0, R0, beta, sigma, gamma, days)
    plot_seir(t, data)

if __name__ == "__main__":
    main()

    #python seir_wrapper.py -N 1000 -beta 1.34 -I0 1
